(function (window) {
  function Line(selector) {
    this.lines = document.getElementsByClassName(selector);
    this.direction = 1;
    this.delay = 1;
    this.overideDelay = 1;
    this.shown = false;

    this.show = function () {
      if (!this.lines) return;
      this.shown = true;
      for (var t, e = 0, s = this.lines.length; e < s; e++) {
        (t = this.direction > 0 ? 0.11 * e : 0.11 * (s - 1 - e)),
        TweenLite.fromTo(this.lines[e], (1.5 - t) / 2, {
          y: 200 * this.direction + "%",
          opacity: 0
        }, {
          y: "0%",
          opacity: 1,
          ease: Quart.easeOut,
          delay: 0.7 * this.delay * this.overideDelay + t
        });
      }
    };

    this.hide = function () {
      if (!this.lines) return;
      this.shown = false;
      for (var t, e, s = 0, i = this.lines.length; s < i; s++) {
        this.direction > 0 ?
          ((t = 0.06 * s), (e = this.direction * (-70 * (i - 1 - s) - 200))) :
          ((t = 0.06 * (i - 1 - s)), (e = this.direction * (-70 * s - 200))),
          TweenLite.to(this.lines[s], 1, {
            y: e + "%",
            ease: Quart.easeInOut,
            delay: this.delay * this.overideDelay * 0.5
          }),
          TweenLite.to(this.lines[s], 0.4, {
            opacity: 0,
            ease: Quart.easeOut,
            delay: 0.5 + this.delay * this.overideDelay + 2.5 * t
          });
      }
    };

    this.update = function () {
      if (!this.shown) return;
      if (!this.lines) return;
      for (var t, e = 0, s = this.lines.length; e < s; e++) {
        (t = this.direction > 0 ? 0.11 * e : 0.11 * (s - 1 - e)),
        TweenLite.to(this.lines[e], (1.5 - t) / 2, {
          y: "0%",
          opacity: 1,
          ease: Quart.easeOut,
          delay: 0.7 * this.delay * this.overideDelay + t
        });
      }
    };
  }

  function homeFadeInOut(selectorId, delayFrom, delayTo) {
    this.item = document.getElementById(selectorId);
    this.shown = false;
    this.show = function () {
      if (!this.item) return;
      this.shown = true;
      TweenLite.fromTo(
        this.item,
        3, {
          opacity: 0
        }, {
          opacity: 1,
          ease: Quart.easeOut,
          delay: delayFrom
        }
      );
    };

    this.hide = function () {
      if (!this.item) return;
      this.shown = false;
      TweenLite.to(this.item, 0.375, {
        opacity: 0,
        ease: Quart.easeOut,
        delay: delayTo
      });
    };

    this.update = function () {
      if (!this.shown) return;
      TweenLite.to(
        this.item,
        0.2, {
          opacity: 1,
          ease: Quart.easeOut
        }
      );
    };
  }

  function homeFadeInOutCurtain(selectorId, delayFrom, delayTo) {
    this.item = document.getElementById(selectorId);
    this.shown = false;
    this.show = function () {
      if (!this.item) return;
      this.shown = true;
      TweenLite.fromTo(this.item,
        3, {
          opacity: 0,
          zIndex: -1
        }, {
          opacity: 1,
          zIndex: 1000,
          ease: Quart.easeOut,
          delay: delayFrom
        }
      );
    };

    this.hide = function () {
      if (!this.item) return;
      this.shown = false;
      TweenLite.fromTo(this.item,
        3, {
          opacity: 1,
          zIndex: 1000
        }, {
          opacity: 0,
          zIndex: -1,
          ease: Quart.easeOut,
          delay: delayTo
        }
      );
    };

    this.update = function () {
      if (!this.shown) return;
      TweenLite.to(this.item,
        0.2, {
          opacity: 1,
          zIndex: 1000,
          ease: Quart.easeOut
        }
      );
    };
  }

  function Page(name, content) {
    this.name = name;
    this.page = document.getElementById(name);
    this.show = function (direction) {
      const button = document.getElementById(`menu-item-${this.name}`);
      if (button) button.classList.add("active");

      TweenLite.fromTo(this.page,
        0.7, {
          y: direction * window.innerHeight
        }, {
          y: 0,
          ease: Quint.easeInOut
        }
      );
      if (content) {
        if (Array.isArray(content)) {
          for (let i = 0; i < content.length; i++) {
            content[i].show();
          }
        } else {
          content.show();
        }
      }
    };
    this.hide = function (direction) {
      const button = document.getElementById(`menu-item-${this.name}`);
      if (button) button.classList.remove("active");
      if (content) {
        if (Array.isArray(content)) {
          for (let i = 0; i < content.length; i++) {
            content[i].hide();
          }
        } else {
          content.hide();
        }
      }
      TweenLite.fromTo(this.page,
        0.7, {
          y: 0
        }, {
          y: direction * -window.innerHeight,
          ease: Quint.easeInOut
        }
      );
    };
    this.update = function (index) {
      if (content) {
        if (Array.isArray(content)) {
          for (let i = 0; i < content.length; i++) {
            content[i].update();
          }
        } else {
          content.update();
        }
      }
      TweenLite.to(this.page,
        0, {
          y: index * window.innerHeight,
          ease: Quint.easeInOut
        }
      );
    };
  }



  function detectSwipe(el, func) {
    let swipe_det = {
      sX: 0,
      sY: 0,
      eX: 0,
      eY: 0
    };
    let min_x = 30,
      max_x = 30,
      min_y = 50,
      max_y = 60,
      direc = "",
      element = document.getElementById(el);

    element.addEventListener(
      "touchstart",
      (e) => {
        swipe_det.sX = e.touches[0].screenX;
        swipe_det.sY = e.touches[0].screenY;
      },
      false
    );
    element.addEventListener(
      "touchmove",
      (e) => {
        e.preventDefault();
        var t = e.touches[0];
        swipe_det.eX = t.screenX;
        swipe_det.eY = t.screenY;
      },
      false
    );
    element.addEventListener(
      "touchend",
      function (e) {
        if (
          (swipe_det.eX - min_x > swipe_det.sX ||
            swipe_det.eX + min_x < swipe_det.sX) &&
          swipe_det.eY < swipe_det.sY + max_y &&
          swipe_det.sY > swipe_det.eY - max_y &&
          swipe_det.eX > 0
        ) {
          if (swipe_det.eX > swipe_det.sX) direc = "r";
          else direc = "l";
        } else if (
          (swipe_det.eY - min_y > swipe_det.sY ||
            swipe_det.eY + min_y < swipe_det.sY) &&
          swipe_det.eX < swipe_det.sX + max_x &&
          swipe_det.sX > swipe_det.eX - max_x &&
          swipe_det.eY > 0
        ) {
          if (swipe_det.eY > swipe_det.sY) direc = "d";
          else direc = "u";
        }

        if (direc != "") {
          if (typeof func == "function") func(el, direc);
        }
        direc = "";
        swipe_det.sX = 0;
        swipe_det.sY = 0;
        swipe_det.eX = 0;
        swipe_det.eY = 0;
      },
      false
    );
  }

  const galeryMove = (el, d) => d === "l" || d === "u" ? galeryScroller.next() : galeryScroller.previous();

  function updateGalery(e) {
    const galeryItemWrapper = document.getElementsByClassName(
      "galery-item-wrapper"
    );
    const baseWidth = galeryItemWrapper[0].clientWidth;
    for (let i = 0; i < galeryItemWrapper.length; i++) {
      TweenLite.to(galeryItemWrapper[i], 0.1, {
        x: `${i * baseWidth - counter * baseWidth}`,
        ease: Quart.easeInOut
      });
    }
  }

  let counter = 0;

  function showGaleryItem(direction) {
    if (counter === totalImages - 1 && direction === "next") return;
    if (counter === 0 && direction === "previous") return;
    direction === "next" ? counter++ : counter--;
    const galeryItemWrapper = document.getElementsByClassName(
      "galery-item-wrapper"
    );
    const baseWidth = galeryItemWrapper[0].clientWidth;
    for (let i = 0; i < galeryItemWrapper.length; i++) {
      setTimeout(() => {
        TweenLite.to(galeryItemWrapper[i], 0.3, {
          x: direction === "next" ? `-=${baseWidth}` : `+=${baseWidth}`,
          ease: Quart.easeInOut
        });
      }, 0)
    }
  }

  //
  // logic
  //


  const pages = {
    shownPage: "home",
    home: new Page("home", [
      new homeFadeInOut("maxim", 1, 0),
      new homeFadeInOut("photograph", 2, 0)
    ]),
    galery: new Page("galery", []),
    about: new Page("about"),
    prices: new Page("prices"),
    contacts: new Page("contacts"),
    order: ["home", "galery", "about", "prices", "contacts"],

    initialize: function () {
      const curtain = new homeFadeInOutCurtain("curtain", 1);
      curtain.hide();
      this.home.show();
      const header = new homeFadeInOut("header", 3);
      header.show();
      this.addEventListenersOnButton();
      this.addEventListenersOnLogo();
      this.addEventListenersOnPageResizeOrChangeOrientation();
    },

    next: function (direction) {
      const nextPage = this.order[this.order.indexOf(this.shownPage) + 1];
      if (!nextPage) return false;
      this[this.shownPage].hide(direction);
      this[nextPage].show(direction);
      this.shownPage = nextPage;
      return true;
    },

    previous: function (direction) {
      const prevPage = this.order[this.order.indexOf(this.shownPage) - 1];
      if (!prevPage) return false;
      this[this.shownPage].hide(direction);
      this[prevPage].show(direction);
      this.shownPage = prevPage;
      return true;
    },

    showCurrentPage: function (page, direction) {
      if (!page) return false;
      this[this.shownPage].hide(direction);
      this[page].show(direction);
      this.shownPage = page;
      return true;
    },

    addEventListenersOnButton: function () {
      const showPage = function (e) {
        const id = e.target.id.replace("menu-item-", "");
        if (id === this.shownPage) return;
        const direction =
          this.order.indexOf(this.shownPage) > this.order.indexOf(id) ? -1 : 1;
        this.showCurrentPage(id, direction);
      };
      const buttons = document.getElementsByClassName("menu-item");
      for (let b = 0; b < buttons.length; b++) {
        const button = buttons[b];
        button.addEventListener("click", showPage.bind(this));
        button.addEventListener("touch", showPage.bind(this));
      }
    },

    addEventListenersOnLogo() {
      var logoHandler = document.getElementById("logo");
      logoHandler.addEventListener("click", e => {
        if ("home" === this.shownPage) return;
        const direction =
          this.order.indexOf(this.shownPage) > this.order.indexOf("home") ?
          -1 :
          1;
        this.showCurrentPage("home", direction);
      });
    },

    addEventListenersOnPageResizeOrChangeOrientation: function () {
      window.addEventListener("resize", update.bind(this));
      window.addEventListener("orientationchange", update.bind(this));

      function update(e) {
        const currentPageIndex = this.order.indexOf(this.shownPage);
        for (let index = 0; index < this.order.length; index++) {
          const pageName = this.order[index];
          this[pageName].update(index - currentPageIndex);
        }
      }
    }
  };

  pages.initialize();

  const galery = document.getElementById("galery");
  const galeryWrapper = document.getElementById("galery-wrapper");
  const mainPane = document.getElementById("main-pane");

  let upDown = false;
  let ts;
  let wait = false;
  const waitFn = e => {
    e.stopPropagation();
    e.preventDefault();
    if (!wait) {
      wait = true;
      setTimeout(() => {
        wait = false;
        upDown ? pages.next(1) : pages.previous(-1);
      }, 300);
    }
  };

  const totalImages = 38;
  for (let i = 0; i < totalImages; i++) {
    setTimeout(() => {
      let div = document.createElement("div");
      div.classList.add("galery-item-wrapper");

      let img = new Image();
      img.alt = "Картинка пока еще не загрузилась";
      img.src = `galery/${i + 1}.JPG`;
      img.classList.add("galery-item");

      div.appendChild(img);
      galeryWrapper.appendChild(div);
      TweenLite.set(div, {
        x: i * div.clientWidth
      });
    }, 0);
  }

  const galeryScroller = {
    next: function (e) {
      if (e) {
        e.stopPropagation();
        e.preventDefault();
      }
      if (!wait) {
        wait = true;
        setTimeout(() => {
          wait = false;
          showGaleryItem("next");
        }, 300);
      }
    },
    previous: function (e) {
      if (e) {
        e.stopPropagation();
        e.preventDefault();
      }
      if (!wait) {
        wait = true;
        setTimeout(() => {
          wait = false;
          showGaleryItem("previous");
        }, 300);
      }
    }
  };

  galery.addEventListener("mousewheel", e => e.stopPropagation());
  galery.addEventListener("touchmove", e => e.stopPropagation());
  detectSwipe("galery", galeryMove);
  galery.addEventListener("mousewheel", e =>
    e.deltaY > 0 ? galeryScroller.next() : galeryScroller.previous()
  );

  window.addEventListener("mousewheel", waitFn);
  window.addEventListener("touchmove", waitFn);
  window.addEventListener("resize", updateGalery);
  window.addEventListener("orientationchange", updateGalery);

  mainPane.addEventListener("touchstart", (e) => ts = e.touches[0].clientY);
  mainPane.addEventListener("touchmove", (e) => upDown = (ts > e.changedTouches[0].clientY) ? true : false);
  mainPane.addEventListener("touchmove", waitFn);
  mainPane.addEventListener("mousewheel", (e) => upDown = (e.deltaY > 0) ? true : false);
  mainPane.addEventListener("mousewheel", waitFn);

})(window)