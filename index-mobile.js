(function (window) {
  function homeFadeInOut(selectorId, delayFrom, delayTo) {
    this.item = document.getElementById(selectorId);
    this.show = function () {
      if (!this.item) return;
      TweenLite.fromTo(
        this.item,
        3, {
          opacity: 0
        }, {
          opacity: 1,
          ease: Quart.easeOut,
          delay: delayFrom
        }
      );
    };

    this.hide = function () {
      if (!this.item) return;
      TweenLite.to(this.item, 0.375, {
        opacity: 0,
        ease: Quart.easeOut,
        delay: delayTo
      });
    };
  }

  function homeFadeInOutCurtain(selectorId, delayFrom, delayTo) {
    this.item = document.getElementById(selectorId);
    this.hide = function () {
      if (!this.item) return;
      TweenLite.fromTo(this.item,
        3, {
          opacity: 1,
          zIndex: 10000
        }, {
          opacity: 0,
          zIndex: -1,
          ease: Quart.easeOut,
          delay: delayTo
        }
      );
    };
  }


  new homeFadeInOutCurtain("curtain", 1).hide();
  new homeFadeInOut("maxim", 0, 0).show();
  new homeFadeInOut("photograph", 1, 0).show();
  new homeFadeInOut("header", 1.2).show();

  const galeryWrapper = document.getElementById("galery-wrapper");
  const mainPane = document.getElementById("main-pane");
  const totalImages = 38;

  for (let i = 0; i < totalImages; i++) {
    setTimeout(() => {
      let img = new Image();
      img.alt = "Картинка пока еще не загрузилась";
      img.src = `galery/${i + 1}.JPG`;
      img.classList.add("galery-item");
      galeryWrapper.appendChild(img);

      img.addEventListener('click', (e) => {
        openOverlay(e, i)
      })
    }, 0);
  }



  document.querySelectorAll('.menu-item').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
      e.preventDefault();
      TweenLite.to(mainPane,
        1, {
          scrollTo: this.getAttribute('href'),
          ease: Quart.easeOut
        }
      );
    });
  });

  let activepage = 'home';
  const pages = ['home', 'galery', 'about', 'prices', 'contacts'];
  mainPane.addEventListener('scroll', (e) => {
    const clientHeight = e.target.clientHeight;
    const top = e.target.scrollTop;

    for (let i = 0; i < pages.length; i++) {
      const element = document.getElementById(pages[i]);
      const elemTop = element.offsetTop;
      const elemBottom = element.offsetTop + element.scrollHeight;
      if (elemTop > top && elemTop < clientHeight / 2 + top ||
        elemBottom > clientHeight / 2 + top && elemBottom < clientHeight + top) {
        activepage = pages[i];
      }
    }

    const buttons = document.getElementsByClassName(`menu-item`);
    for (let i = 0; i < buttons.length; i++) {
      buttons[i].classList.remove("active");
    }
    if (activepage !== 'home') {
      const activeButton = document.getElementById(`menu-item-${activepage}`);
      activeButton.classList.add('active');
    }
  });


  const overlay = document.getElementsByClassName('overlay')[0];
  const closeButton = document.getElementsByClassName('overlay-close-button')[0];

  let lastX = 0,
    lastY = 0;
  let shownImage;
  const openOverlay = (event, imgIndex) => {
    lastX = event.clientX;
    lastY = event.clientY;
    setTimeout(() => {
      shownImage = new Image();
      shownImage.alt = "Картинка пока еще не загрузилась";
      shownImage.src = `galery/${imgIndex + 1}.JPG`;
      shownImage.classList.add("galery-item-full");
      overlay.appendChild(shownImage);



    }, 0);


    TweenLite.fromTo(
      overlay,
      0.375, {
        top: lastY,
        left: lastX,
        width: 0,
        height: 0,
        opacity: 0
      }, {
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        opacity: 1,
        ease: Quart.easeOut,
      }
    );
  }

  const hideOverlay = () => {
    TweenLite.to(overlay, 0.375, {
      top: lastY,
      left: lastX,
      width: 0,
      height: 0,
      opacity: 0
    });
    lastX = 0;
    lastY = 0;
    shownImage.remove();
  }

  closeButton.addEventListener('click', hideOverlay)


})(window);